FROM ubuntu:bionic

MAINTAINER Léo Chéron <leo@cheron.works>

# Update sources
# COPY sources.list /etc/apt/

# fix term not set
RUN echo 'export TERM=xterm' >> ~/.bashrc

#-----------------*
# Language support
#-----------------*
RUN apt-get update && apt-get install locales && locale-gen en_US.UTF-8
ENV LANG=en_US.UTF-8 \
	LANGUAGE=en_US:en \
	LC_ALL=en_US.UTF-8

#-----------------*
# Add ssh user
#-----------------*
RUN apt-get install -y openssh-server rsync && \
	adduser --disabled-password --gecos "" deploy && \
	mkdir /root/.ssh
	# mkdir /home/deploy/.ssh && chown deploy:www-data /home/deploy/.ssh && \

#-----------------*
# Utils
#-----------------*
RUN apt-get install -y htop vim git-core zsh && \
	wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | bash && \
	chsh -s `which zsh` && \
	usermod -g www-data deploy

#-----------------*
# Install apache
#-----------------*
# Manually set up the apache environment variables
ENV APACHE_RUN_USER=www-data \
	APACHE_RUN_GROUP=www-data \
	APACHE_LOG_DIR=/var/log/apache2 \
	APACHE_PID_FILE=/var/run/apache2.pid \
	APACHE_RUN_DIR=/var/run/apache2 \
	APACHE_LOCK_DIR=/var/lock/apache2

RUN apt-get install -y apache2 && \
	wget https://mirrors.edge.kernel.org/ubuntu/pool/multiverse/liba/libapache-mod-fastcgi/libapache2-mod-fastcgi_2.4.7~0910052141-1.2_amd64.deb && dpkg -i libapache2-mod-fastcgi_2.4.7~0910052141-1.2_amd64.deb && \
	mkdir -p $APACHE_RUN_DIR $APACHE_LOCK_DIR $APACHE_LOG_DIR && \
	a2enmod rewrite expires headers vhost_alias deflate fastcgi actions remoteip setenvif && \
	chown www-data:www-data -R /var/lib/apache2/fastcgi && \
	chown -R www-data:www-data /var/www

# Setup vhost
COPY ./vhost.conf /etc/apache2/sites-available/
RUN a2ensite vhost.conf && a2dissite 000-default.conf

# Setup apache
COPY apache2/conf-enabled/* /etc/apache2/conf-enabled/
COPY apache2/mods-enabled/* /etc/apache2/mods-enabled/
# RUN rm /etc/apache2/conf-enabled/other-vhosts-access-log.conf
# setup logs
RUN ln -sf /dev/stdout /var/log/apache2/other_vhosts_access.log && \
	ln -sf /dev/stderr /var/log/apache2/error.log

#-----------------*
# Install php
#-----------------*
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y software-properties-common && add-apt-repository ppa:ondrej/php && apt-get update && \
	apt-get install -y php7.4 php7.4-fpm php7.4-gd php7.4-mysql php7.4-curl php7.4-intl php-pear php-imagick php7.4-imap php7.4-sqlite3 php7.4-tidy php7.4-xmlrpc php7.4-mbstring \
	php-memcached php7.4-opcache

COPY httpd.conf /usr/local/apache2/conf/
COPY php.ini /etc/php/7.4/fpm/
COPY www.conf /etc/php/7.4/fpm/pool.d/
COPY opcache.ini /etc/php/7.4/mods-available/

#-----------------*
# Cleanup
#-----------------*
RUN apt-get autoremove -y && \
	apt-get clean all

EXPOSE 22 80 443

CMD service ssh start && service php7.4-fpm start && /usr/sbin/apache2ctl -D FOREGROUND

WORKDIR /var/www/